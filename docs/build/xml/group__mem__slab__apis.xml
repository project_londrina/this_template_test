<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.9.1" xml:lang="en-US">
  <compounddef id="group__mem__slab__apis" kind="group">
    <compoundname>mem_slab_apis</compoundname>
    <title>Memory Slab APIs</title>
      <sectiondef kind="func">
      <memberdef kind="function" id="group__mem__slab__apis_1ga094a8f173f287e29bb287119c26889d1" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>int</type>
        <definition>int k_mem_slab_init</definition>
        <argsstring>(struct k_mem_slab *slab, void *buffer, size_t block_size, uint32_t num_blocks)</argsstring>
        <name>k_mem_slab_init</name>
        <param>
          <type>struct k_mem_slab *</type>
          <declname>slab</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>block_size</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>num_blocks</declname>
        </param>
        <briefdescription>
<para>Initialize a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>Initializes a memory slab, prior to its first use.</para>
<para>The memory slab&apos;s buffer contains <emphasis>slab_num_blocks</emphasis> memory blocks that are <emphasis>slab_block_size</emphasis> bytes long. The buffer must be aligned to an N-byte boundary matching a word boundary, where N is a power of 2 (i.e. 4 on 32-bit systems, 8, 16, ...). To ensure that each memory block is similarly aligned to this boundary, <emphasis>slab_block_size</emphasis> must also be a multiple of N.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>slab</parametername>
</parameternamelist>
<parameterdescription>
<para>Address of the memory slab. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to buffer used for the memory blocks. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>block_size</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of each memory block (in bytes). </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>num_blocks</parametername>
</parameternamelist>
<parameterdescription>
<para>Number of memory blocks.</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<parameterlist kind="retval"><parameteritem>
<parameternamelist>
<parametername>0</parametername>
</parameternamelist>
<parameterdescription>
<para>on success </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>-EINVAL</parametername>
</parameternamelist>
<parameterdescription>
<para>invalid data supplied </para>
</parameterdescription>
</parameteritem>
</parameterlist>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4816" column="12" declfile="E:/docs_cb6/include/kernel.h" declline="4816" declcolumn="12"/>
      </memberdef>
      <memberdef kind="function" id="group__mem__slab__apis_1gab16a46d8394aca18de740ad044a8734a" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>int</type>
        <definition>int k_mem_slab_alloc</definition>
        <argsstring>(struct k_mem_slab *slab, void **mem, k_timeout_t timeout)</argsstring>
        <name>k_mem_slab_alloc</name>
        <param>
          <type>struct k_mem_slab *</type>
          <declname>slab</declname>
        </param>
        <param>
          <type>void **</type>
          <declname>mem</declname>
        </param>
        <param>
          <type>k_timeout_t</type>
          <declname>timeout</declname>
        </param>
        <briefdescription>
<para>Allocate memory from a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine allocates a memory block from a memory slab.</para>
<para><simplesect kind="note"><para>Can be called by ISRs, but <emphasis>timeout</emphasis> must be set to K_NO_WAIT.</para>
</simplesect>
<parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>slab</parametername>
</parameternamelist>
<parameterdescription>
<para>Address of the memory slab. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mem</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to block address area. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeout</parametername>
</parameternamelist>
<parameterdescription>
<para>Non-negative waiting period to wait for operation to complete. Use K_NO_WAIT to return without waiting, or K_FOREVER to wait as long as necessary.</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<parameterlist kind="retval"><parameteritem>
<parameternamelist>
<parametername>0</parametername>
</parameternamelist>
<parameterdescription>
<para>Memory allocated. The block address area pointed at by <emphasis>mem</emphasis> is set to the starting address of the memory block. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>-ENOMEM</parametername>
</parameternamelist>
<parameterdescription>
<para>Returned without waiting. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>-EAGAIN</parametername>
</parameternamelist>
<parameterdescription>
<para>Waiting period timed out. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>-EINVAL</parametername>
</parameternamelist>
<parameterdescription>
<para>Invalid data supplied </para>
</parameterdescription>
</parameteritem>
</parameterlist>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4838" column="12" declfile="E:/docs_cb6/include/kernel.h" declline="4838" declcolumn="12"/>
      </memberdef>
      <memberdef kind="function" id="group__mem__slab__apis_1ga97915aee5a59e19f8b28185eed93aac7" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void</type>
        <definition>void k_mem_slab_free</definition>
        <argsstring>(struct k_mem_slab *slab, void **mem)</argsstring>
        <name>k_mem_slab_free</name>
        <param>
          <type>struct k_mem_slab *</type>
          <declname>slab</declname>
        </param>
        <param>
          <type>void **</type>
          <declname>mem</declname>
        </param>
        <briefdescription>
<para>Free memory allocated from a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine releases a previously allocated memory block back to its associated memory slab.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>slab</parametername>
</parameternamelist>
<parameterdescription>
<para>Address of the memory slab. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mem</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to block address area (as set by <ref refid="group__mem__slab__apis_1gab16a46d8394aca18de740ad044a8734a" kindref="member">k_mem_slab_alloc()</ref>).</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>N/A </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4852" column="13" declfile="E:/docs_cb6/include/kernel.h" declline="4852" declcolumn="13"/>
      </memberdef>
      <memberdef kind="function" id="group__mem__slab__apis_1gac76b96d7055e4ad94765c93530dd0720" prot="public" static="yes" const="no" explicit="no" inline="yes" virt="non-virtual">
        <type>uint32_t</type>
        <definition>static uint32_t k_mem_slab_num_used_get</definition>
        <argsstring>(struct k_mem_slab *slab)</argsstring>
        <name>k_mem_slab_num_used_get</name>
        <param>
          <type>struct k_mem_slab *</type>
          <declname>slab</declname>
        </param>
        <briefdescription>
<para>Get the number of used blocks in a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine gets the number of memory blocks that are currently allocated in <emphasis>slab</emphasis>.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>slab</parametername>
</parameternamelist>
<parameterdescription>
<para>Address of the memory slab.</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Number of allocated memory blocks. </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4864" column="24" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="4864" bodyend="4867"/>
      </memberdef>
      <memberdef kind="function" id="group__mem__slab__apis_1gae0e949c1c3476dd57bc0c0ed627d2346" prot="public" static="yes" const="no" explicit="no" inline="yes" virt="non-virtual">
        <type>uint32_t</type>
        <definition>static uint32_t k_mem_slab_max_used_get</definition>
        <argsstring>(struct k_mem_slab *slab)</argsstring>
        <name>k_mem_slab_max_used_get</name>
        <param>
          <type>struct k_mem_slab *</type>
          <declname>slab</declname>
        </param>
        <briefdescription>
<para>Get the number of maximum used blocks so far in a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine gets the maximum number of memory blocks that were allocated in <emphasis>slab</emphasis>.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>slab</parametername>
</parameternamelist>
<parameterdescription>
<para>Address of the memory slab.</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Maximum number of allocated memory blocks. </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4879" column="24" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="4879" bodyend="4887"/>
      </memberdef>
      <memberdef kind="function" id="group__mem__slab__apis_1gae87577e2873cf746db69216a82f94aea" prot="public" static="yes" const="no" explicit="no" inline="yes" virt="non-virtual">
        <type>uint32_t</type>
        <definition>static uint32_t k_mem_slab_num_free_get</definition>
        <argsstring>(struct k_mem_slab *slab)</argsstring>
        <name>k_mem_slab_num_free_get</name>
        <param>
          <type>struct k_mem_slab *</type>
          <declname>slab</declname>
        </param>
        <briefdescription>
<para>Get the number of unused blocks in a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine gets the number of memory blocks that are currently unallocated in <emphasis>slab</emphasis>.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>slab</parametername>
</parameternamelist>
<parameterdescription>
<para>Address of the memory slab.</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Number of unallocated memory blocks. </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4899" column="24" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="4899" bodyend="4902"/>
      </memberdef>
      </sectiondef>
      <sectiondef kind="define">
      <memberdef kind="define" id="group__mem__slab__apis_1ga60bc92eee58fcc5f121b8e4d82eaa69e" prot="public" static="no">
        <name>K_MEM_SLAB_DEFINE</name>
        <param><defname>name</defname></param>
        <param><defname>slab_block_size</defname></param>
        <param><defname>slab_num_blocks</defname></param>
        <param><defname>slab_align</defname></param>
        <initializer>	char __noinit __aligned(WB_UP(slab_align)) \
	   _k_mem_slab_buf_##name[(slab_num_blocks) * WB_UP(slab_block_size)]; \
	Z_STRUCT_SECTION_ITERABLE(k_mem_slab, name) = \
		Z_MEM_SLAB_INITIALIZER(name, _k_mem_slab_buf_##name, \
					WB_UP(slab_block_size), slab_num_blocks)</initializer>
        <briefdescription>
<para>Statically define and initialize a memory slab. </para>
        </briefdescription>
        <detaileddescription>
<para>The memory slab&apos;s buffer contains <emphasis>slab_num_blocks</emphasis> memory blocks that are <emphasis>slab_block_size</emphasis> bytes long. The buffer is aligned to a <emphasis>slab_align</emphasis> -byte boundary. To ensure that each memory block is similarly aligned to this boundary, <emphasis>slab_block_size</emphasis> must also be a multiple of <emphasis>slab_align</emphasis>.</para>
<para>The memory slab can be accessed outside the module where it is defined using:</para>
<para><programlisting><codeline><highlight class="normal">extern<sp/>struct<sp/>k_mem_slab<sp/>&lt;name&gt;;<sp/></highlight></codeline>
</programlisting></para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>name</parametername>
</parameternamelist>
<parameterdescription>
<para>Name of the memory slab. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>slab_block_size</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of each memory block (in bytes). </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>slab_num_blocks</parametername>
</parameternamelist>
<parameterdescription>
<para>Number memory blocks. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>slab_align</parametername>
</parameternamelist>
<parameterdescription>
<para>Alignment of the memory slab&apos;s buffer (power of 2). </para>
</parameterdescription>
</parameteritem>
</parameterlist>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4788" column="9" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="4788" bodyend="-1"/>
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>
