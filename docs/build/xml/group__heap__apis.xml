<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.9.1" xml:lang="en-US">
  <compounddef id="group__heap__apis" kind="group">
    <compoundname>heap_apis</compoundname>
    <title>Heap APIs</title>
    <innerclass refid="structk__heap" prot="public">k_heap</innerclass>
      <sectiondef kind="func">
      <memberdef kind="function" id="group__heap__apis_1ga9273e06dc8d6a351499f2f5abfdcb39f" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void</type>
        <definition>void k_heap_init</definition>
        <argsstring>(struct k_heap *h, void *mem, size_t bytes)</argsstring>
        <name>k_heap_init</name>
        <param>
          <type>struct <ref refid="structk__heap" kindref="compound">k_heap</ref> *</type>
          <declname>h</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>mem</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>bytes</declname>
        </param>
        <briefdescription>
<para>Initialize a <ref refid="structk__heap" kindref="compound">k_heap</ref>. </para>
        </briefdescription>
        <detaileddescription>
<para>This constructs a synchronized <ref refid="structk__heap" kindref="compound">k_heap</ref> object over a memory region specified by the user. Note that while any alignment and size can be passed as valid parameters, internal alignment restrictions inside the inner sys_heap mean that not all bytes may be usable as allocated memory.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>h</parametername>
</parameternamelist>
<parameterdescription>
<para>Heap struct to initialize </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mem</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to memory. </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>bytes</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of memory region, in bytes </para>
</parameterdescription>
</parameteritem>
</parameterlist>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4932" column="6" declfile="E:/docs_cb6/include/kernel.h" declline="4932" declcolumn="6"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1ga3b197aee88cde2f1c9ed7e13e8b29f79" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void *</type>
        <definition>void* k_heap_aligned_alloc</definition>
        <argsstring>(struct k_heap *h, size_t align, size_t bytes, k_timeout_t timeout)</argsstring>
        <name>k_heap_aligned_alloc</name>
        <param>
          <type>struct <ref refid="structk__heap" kindref="compound">k_heap</ref> *</type>
          <declname>h</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>align</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>bytes</declname>
        </param>
        <param>
          <type>k_timeout_t</type>
          <declname>timeout</declname>
        </param>
        <briefdescription>
<para>Allocate aligned memory from a <ref refid="structk__heap" kindref="compound">k_heap</ref>. </para>
        </briefdescription>
        <detaileddescription>
<para>Behaves in all ways like k_heap_alloc(), except that the returned memory (if available) will have a starting address in memory which is a multiple of the specified power-of-two alignment value in bytes. The resulting memory can be returned to the heap using <ref refid="group__heap__apis_1ga6cf917a0b3d91a0101192bd4808ada9c" kindref="member">k_heap_free()</ref>.</para>
<para><simplesect kind="note"><para>Can be called by ISRs, but <emphasis>timeout</emphasis> must be set to K_NO_WAIT.</para>
</simplesect>
<parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>h</parametername>
</parameternamelist>
<parameterdescription>
<para>Heap from which to allocate </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>align</parametername>
</parameternamelist>
<parameterdescription>
<para>Alignment in bytes, must be a power of two </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>bytes</parametername>
</parameternamelist>
<parameterdescription>
<para>Number of bytes requested </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeout</parametername>
</parameternamelist>
<parameterdescription>
<para>How long to wait, or K_NO_WAIT </para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Pointer to memory the caller can now use </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4950" column="6" declfile="E:/docs_cb6/include/kernel.h" declline="4950" declcolumn="6"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1gabbd125c1ac8e4f833a54b3bc15364dd5" prot="public" static="yes" const="no" explicit="no" inline="yes" virt="non-virtual">
        <type>void *</type>
        <definition>static void* k_heap_alloc</definition>
        <argsstring>(struct k_heap *h, size_t bytes, k_timeout_t timeout)</argsstring>
        <name>k_heap_alloc</name>
        <param>
          <type>struct <ref refid="structk__heap" kindref="compound">k_heap</ref> *</type>
          <declname>h</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>bytes</declname>
        </param>
        <param>
          <type>k_timeout_t</type>
          <declname>timeout</declname>
        </param>
        <briefdescription>
<para>Allocate memory from a <ref refid="structk__heap" kindref="compound">k_heap</ref>. </para>
        </briefdescription>
        <detaileddescription>
<para>Allocates and returns a memory buffer from the memory region owned by the heap. If no memory is available immediately, the call will block for the specified timeout (constructed via the standard timeout API, or K_NO_WAIT or K_FOREVER) waiting for memory to be freed. If the allocation cannot be performed by the expiration of the timeout, NULL will be returned.</para>
<para><simplesect kind="note"><para>Can be called by ISRs, but <emphasis>timeout</emphasis> must be set to K_NO_WAIT.</para>
</simplesect>
<parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>h</parametername>
</parameternamelist>
<parameterdescription>
<para>Heap from which to allocate </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>bytes</parametername>
</parameternamelist>
<parameterdescription>
<para>Desired size of block to allocate </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeout</parametername>
</parameternamelist>
<parameterdescription>
<para>How long to wait, or K_NO_WAIT </para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A pointer to valid heap memory, or NULL </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4970" column="20" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="4970" bodyend="4974"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1ga6cf917a0b3d91a0101192bd4808ada9c" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void</type>
        <definition>void k_heap_free</definition>
        <argsstring>(struct k_heap *h, void *mem)</argsstring>
        <name>k_heap_free</name>
        <param>
          <type>struct <ref refid="structk__heap" kindref="compound">k_heap</ref> *</type>
          <declname>h</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>mem</declname>
        </param>
        <briefdescription>
<para>Free memory allocated by k_heap_alloc() </para>
        </briefdescription>
        <detaileddescription>
<para>Returns the specified memory block, which must have been returned from k_heap_alloc(), to the heap for use by other callers. Passing a NULL block is legal, and has no effect.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>h</parametername>
</parameternamelist>
<parameterdescription>
<para>Heap to which to return the memory </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mem</parametername>
</parameternamelist>
<parameterdescription>
<para>A valid memory block, or NULL </para>
</parameterdescription>
</parameteritem>
</parameterlist>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4986" column="6" declfile="E:/docs_cb6/include/kernel.h" declline="4986" declcolumn="6"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1ga5088aeb1294fdc5ab2dcfef017fd43a6" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void *</type>
        <definition>void* k_aligned_alloc</definition>
        <argsstring>(size_t align, size_t size)</argsstring>
        <name>k_aligned_alloc</name>
        <param>
          <type>size_t</type>
          <declname>align</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>size</declname>
        </param>
        <briefdescription>
<para>Allocate memory from the heap with a specified alignment. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine provides semantics similar to aligned_alloc(); memory is allocated from the heap with a specified alignment. However, one minor difference is that <ref refid="group__heap__apis_1ga5088aeb1294fdc5ab2dcfef017fd43a6" kindref="member">k_aligned_alloc()</ref> accepts any non-zero <computeroutput>size</computeroutput>, wherase aligned_alloc() only accepts a <computeroutput>size</computeroutput> that is an integral multiple of <computeroutput>align</computeroutput>.</para>
<para>Above, aligned_alloc() refers to: C11 standard (ISO/IEC 9899:2011): 7.22.3.1 The aligned_alloc function (p: 347-348)</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>align</parametername>
</parameternamelist>
<parameterdescription>
<para>Alignment of memory requested (in bytes). </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>Amount of memory requested (in bytes).</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Address of the allocated memory if successful; otherwise NULL. </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="5035" column="13" declfile="E:/docs_cb6/include/kernel.h" declline="5035" declcolumn="13"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1ga8c30f092c36816403fb198132a6c841c" prot="public" static="yes" const="no" explicit="no" inline="yes" virt="non-virtual">
        <type>void *</type>
        <definition>static void* k_malloc</definition>
        <argsstring>(size_t size)</argsstring>
        <name>k_malloc</name>
        <param>
          <type>size_t</type>
          <declname>size</declname>
        </param>
        <briefdescription>
<para>Allocate memory from the heap. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine provides traditional malloc() semantics. Memory is allocated from the heap memory pool.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>Amount of memory requested (in bytes).</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Address of the allocated memory if successful; otherwise NULL. </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="5047" column="20" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="5047" bodyend="5050"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1ga79b63cc93b3358cf82d74f40e73b69d5" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void</type>
        <definition>void k_free</definition>
        <argsstring>(void *ptr)</argsstring>
        <name>k_free</name>
        <param>
          <type>void *</type>
          <declname>ptr</declname>
        </param>
        <briefdescription>
<para>Free memory allocated from heap. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine provides traditional free() semantics. The memory being returned must have been allocated from the heap memory pool or k_mem_pool_malloc().</para>
<para>If <emphasis>ptr</emphasis> is NULL, no operation is performed.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>ptr</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to previously allocated memory.</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>N/A </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="5065" column="13" declfile="E:/docs_cb6/include/kernel.h" declline="5065" declcolumn="13"/>
      </memberdef>
      <memberdef kind="function" id="group__heap__apis_1ga8e2782f24e6cdc7902fce1950ace696b" prot="public" static="no" const="no" explicit="no" inline="no" virt="non-virtual">
        <type>void *</type>
        <definition>void* k_calloc</definition>
        <argsstring>(size_t nmemb, size_t size)</argsstring>
        <name>k_calloc</name>
        <param>
          <type>size_t</type>
          <declname>nmemb</declname>
        </param>
        <param>
          <type>size_t</type>
          <declname>size</declname>
        </param>
        <briefdescription>
<para>Allocate memory from heap, array style. </para>
        </briefdescription>
        <detaileddescription>
<para>This routine provides traditional calloc() semantics. Memory is allocated from the heap memory pool and zeroed.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>nmemb</parametername>
</parameternamelist>
<parameterdescription>
<para>Number of elements in the requested array </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of each array element (in bytes).</para>
</parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Address of the allocated memory if successful; otherwise NULL. </para>
</simplesect>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="5078" column="13" declfile="E:/docs_cb6/include/kernel.h" declline="5078" declcolumn="13"/>
      </memberdef>
      </sectiondef>
      <sectiondef kind="define">
      <memberdef kind="define" id="group__heap__apis_1ga795d7f1e6d5b7b19a7a50198d7829a0f" prot="public" static="no">
        <name>K_HEAP_DEFINE</name>
        <param><defname>name</defname></param>
        <param><defname>bytes</defname></param>
        <initializer>	char __aligned(sizeof(void *)) kheap_##name[bytes];	\
	Z_STRUCT_SECTION_ITERABLE(<ref refid="structk__heap" kindref="compound">k_heap</ref>, name) = {		\
		.heap = {					\
			.init_mem = kheap_##name,		\
			.init_bytes = (bytes),			\
		 },						\
	}</initializer>
        <briefdescription>
<para>Define a static <ref refid="structk__heap" kindref="compound">k_heap</ref>. </para>
        </briefdescription>
        <detaileddescription>
<para>This macro defines and initializes a static memory region and <ref refid="structk__heap" kindref="compound">k_heap</ref> of the requested size. After kernel start, &amp;name can be used as if <ref refid="group__heap__apis_1ga9273e06dc8d6a351499f2f5abfdcb39f" kindref="member">k_heap_init()</ref> had been called.</para>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>name</parametername>
</parameternamelist>
<parameterdescription>
<para>Symbol name for the struct <ref refid="structk__heap" kindref="compound">k_heap</ref> object </para>
</parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>bytes</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of memory region, in bytes </para>
</parameterdescription>
</parameteritem>
</parameterlist>
</para>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location file="E:/docs_cb6/include/kernel.h" line="4998" column="9" bodyfile="E:/docs_cb6/include/kernel.h" bodystart="4998" bodyend="-1"/>
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>
